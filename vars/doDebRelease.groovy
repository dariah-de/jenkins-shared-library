/*
 * Publish a DEB Package in APTLY INDY RELEASE Repository
 */

def call(def pname, def ppath, def pversion) {

  echo "====>  Publishing Debian package ${pname} for RELEASE version ${pversion} [indy-releases]"

  sh "PLOC=\$(ls ./${ppath}/*.deb); curl -X POST -F file=@\${PLOC} http://localhost:8008/api/files/${pname}-${pversion}"
  sh "curl -X POST http://localhost:8008/api/repos/indy-releases/file/${pname}-${pversion}"
  sh "curl -X PUT -H 'Content-Type: application/json' --data '{}' http://localhost:8008/api/publish/:./indy"

  sh "rm ./${ppath}/*.deb"
  sh "rm -f ./${ppath}/*.changes"
}
