/*
 * notify a list of downstream projects from a space separated string, if branch is develop
 * or a 'release/* branch'
 */

def call(def jobs) {

  if (env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' || env.BRANCH_NAME.startsWith('release/') || env.BRANCH_NAME == 'feature/photon' || env.BRANCH_NAME == 'v3-develop' || env.BRANCH_NAME == 'v3-main') {
    bnEncoded =  URLEncoder.encode(env.BRANCH_NAME, "utf-8")
    jobs.split(' ').each {
      echo "notify downstream project '${it}' for branch '${env.BRANCH_NAME}' (${bnEncoded})"
      build( job: "${it}/${bnEncoded}", wait: false )
    }
  } else {
    echo "not notifying downstream jobs '${jobs}' for branch '${env.BRANCH_NAME}'"
  }

}

