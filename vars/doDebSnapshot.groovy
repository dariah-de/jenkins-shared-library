/*
 * Publish a DEB Package in APTLY INDY SNAPSHOT Repository
 */

def call(def pname, def ppath, def pversion) {

  echo "====>  Publishing Debian package ${pname} for SNAPSHOT version ${pversion} [indy]"

  sh "PLOC=\$(ls ./${ppath}/*.deb); curl -X POST -F file=@\${PLOC} http://localhost:8008/api/files/${pname}-${pversion}"
  sh "curl -X POST http://localhost:8008/api/repos/indy-snapshots/file/${pname}-${pversion}"
  sh "curl -X PUT -H 'Content-Type: application/json' --data '{}' http://localhost:8008/api/publish/:./indy"

  sh "rm ./${ppath}/*.deb"
  sh "rm -f ./${ppath}/*.changes"
}
