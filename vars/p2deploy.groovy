/*
 * Copy a p2repo from a textgridlab build from {source} to /var/www/p2/{id}/{name}
 * The id is matched from the branch name with repoMatch().
 * If you do not want to copy specific branches edit repoMatch and return false
 */

def call(def source, def name) {
  echo "[p2deploy] branch is ${env.BRANCH_NAME}"
  def repo = repoMatch(env.BRANCH_NAME)
  if(!repo) {
      echo "[p2deploy] no p2repo copied for this branch"
      return
  }
  echo "[p2deploy] repo name matched from branch is ${repo}"
  def target = "/var/www/p2/${repo}/${name}"
  echo "[p2deploy] deploying to local p2repo ${target}"
  sh "mkdir -p ${target}"
  sh "cp -r ${source} ${target}.new"
  sh "mv ${target} ${target}.old"
  sh "mv ${target}.new ${target}"
  sh "rm -rf ${target}.old"
  echo "[p2deploy] done"
}

@NonCPS
def repoMatch(branchName) {
  def matcher = (branchName =~ /^(feature|release|develop|master|v3-develop|v3-main)(\/)?([a-z0-9\.\-]*)$/)
  assert matcher.matches()
  if (matcher[0][1] == 'release' || matcher[0][1] == 'master' || matcher[0][1] == 'v3-main') { // prerelease repo should contain the release finally
    //return "lab-${matcher[0][3]}rc"
    if( matcher[0][1] == 'v3-main' || (matcher[0][1] == 'release' && matcher[0][3].charAt(0) == '3') ) { // the v3 release branches
      return 'v3-prerelease'
    } else {
      return 'prerelease'
    }
  } else if(matcher[0][1] == 'develop') {
    return 'nightly'
//  } else if(matcher[0][1] == 'master') {
//    return false; // we do not keep master branch, as releases are kept separate (on textgridlab.org)
  } else if(matcher[0][3] == 'photon') {
    return "${matcher[0][1]}-${matcher[0][3]}"
  } else if(matcher[0][1] == 'v3-develop') {
    return 'v3-develop'
  }
  //return "lab-${matcher[0][1]}-${matcher[0][3]}"
  return false // just keep p2 repos from release/* or develop branch for now
}
