# jenkins-shared-library

A Library of shared functions for Jenkins. Look at 
https://jenkins.io/doc/book/pipeline/shared-libraries/ 
how this works.

# Other useful links

* https://cleverbuilder.com/articles/jenkins-shared-library/
* https://jenkins.io/blog/2017/10/02/pipeline-templates-with-shared-libraries/
* https://github.com/SAP/jenkins-library
* http://www.aimtheory.com/jenkins/pipeline/continuous-delivery/2017/12/02/jenkins-pipeline-global-shared-library-best-practices.html
